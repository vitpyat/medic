<?php
/**
 * DatabaseSeeder
 *
 * @category  App
 * @package   App\Api
 * @author    Vitaliy Pyatin <mail.pyvil@gmail.com>
 * @cppyright Vitaliy Pyatin
 */

use App\Api\Home as ModelHome;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

/**
 * Class DatabaseSeeder
 */
class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            HomeSeeder::class,
            LanguagesSeeder::class
        ]);
    }
}
