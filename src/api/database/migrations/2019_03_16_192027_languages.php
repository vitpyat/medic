<?php
/**
 * Migration languages
 *
 * @category  App
 * @package   App
 * @author    Vitaliy Pyatin <mail.pyvil@gmail.com>
 * @cppyright Vitaliy Pyatin
 */

use App\Lang;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Languages extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(
            Lang::TABLE_NAME,
            function (Blueprint $table) {
                $table->bigIncrements(Lang::ID);
                $table->char(Lang::CODE);
                $table->string(Lang::NAME);
                $table->boolean(Lang::IS_DEFAULT);
                $table->engine = 'InnoDB';
                $table->charset = 'utf8';
                $table->collation = 'utf8_unicode_ci';
            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(Lang::TABLE_NAME);
    }
}
