<?php

namespace App\Http\Controllers;

class ControllerJson extends Controller
{
    /**
     * Allowed HTTP methods
     *
     * @var array
     */
    protected $_allowedMethods = [
        'GET',
        'POST',
        'PATCH',
        'PUT',
        'DELETE',
        'OPTIONS'
    ];

    /**
     * Allowed origin
     *
     * @var string|array
     */
    protected $_allowedOrigin = '*';

    /**
     * Allowed headers
     *
     * @var array
     */
    protected $_allowedHeaders = [
        'Origin',
        'Content-Type',
        'X-Auth-Token'
    ];

    /**
     * Call Controller's action
     *
     * @param string $method
     * @param array $parameters
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function callAction($method, $parameters)
    {
        return parent::callAction($method, $parameters)
            ->header('Access-Control-Allow-Origin', $this->getAllowedOrigin())
            ->header('Access-Control-Allow-Methods', implode(', ', $this->_allowedMethods))
            ->header('Access-Control-Allow-Headers', implode($this->_allowedHeaders));
    }

    /**
     * Allowed Origin
     *
     * @return array|string
     */
    public function getAllowedOrigin()
    {
        return is_array($this->_allowedOrigin)
            ? implode(', ', $this->_allowedOrigin)
            : $this->_allowedOrigin;
    }

    /**
     * Add allowed method
     *
     * @param string $method
     *
     * @return ControllerJson
     */
    public function addAllowedMethod(string $method): ControllerJson
    {
        $this->_allowedMethods[] = $method;

        return $this;
    }

    /**
     * Add allowed headers
     *
     * @param string $header
     *
     * @return ControllerJson
     */
    public function addAllowedHeaders(string $header): ControllerJson
    {
        $this->_allowedHeaders[] = $header;

        return $this;
    }
}
