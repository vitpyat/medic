<?php
/**
 * Middleware to check language for each request
 *
 * @category  App
 * @package   App\Http\Middleware
 * @author    Vitaliy Pyatin <mail.pyvil@gmail.com>
 * @cppyright Vitaliy Pyatin
 */

namespace App\Http\Middleware;

use App\Lang;
use Closure;
use Illuminate\Support\Facades\Lang as LaravelLang;

/**
 * Class CheckLang
 *
 * @package App\Http\Middleware
 */
class ApplyLanguage
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $paths = explode('/', trim($request->path(), '/'));
        $langInPath = null;
        if (count($paths) > 2 && isset($paths[1])) {
            $langInPath = strtolower($paths[1]);
        }
        $lang = null;
        if ($langInPath) {
            $lang = Lang::query()->where(Lang::CODE, '=', $langInPath)->first();
        }
        if (!$lang) {
            $lang = Lang::query()->where(Lang::IS_DEFAULT, '=', 1)->first();
        }
        $lang = $lang instanceof Lang && $lang->getAttribute(Lang::CODE)
            ? $lang->getAttribute(Lang::CODE)
            : Lang::DEFAULT_LANGUAGE;

        LaravelLang::setLocale($lang);

        return $next($request);
    }
}
