<?php
/**
 * Lang model
 *
 * @category  App
 * @package   App
 * @author    Vitaliy Pyatin <mail.pyvil@gmail.com>
 * @cppyright Vitaliy Pyatin
 */

namespace App;

use Illuminate\Database\Eloquent\Model;


class Lang extends Model
{
    /**#@+
     * Lang codes
     */
    const CODE_UA  = 'ua';
    const CODE_ENG = 'en';
    const CODE_FR  = 'fr';
    const CODE_PL  = 'pl';
    const CODE_RU  = 'ru';
    /**#@-*/

    /**
     * Default language
     */
    const DEFAULT_LANGUAGE = self::CODE_UA;

    /**
     * Table name
     */
    const TABLE_NAME = 'languages';

    /**#@+
     * Columns
     */
    const ID         = 'id';
    const CODE       = 'code';
    const NAME       = 'name';
    const IS_DEFAULT = 'is_default';
    /**#@-*/

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = self::TABLE_NAME;

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;
}
