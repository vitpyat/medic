import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// Pages
import { ContactUsComponent } from './pages/contact-us/contact-us.component';
import { HomeComponent } from './pages/home/home.component';
import { CertificatesComponent } from './pages/certificates/certificates.component';
import { CtgSigmafonComponent } from './pages/ctg-sigmafon/ctg-sigmafon.component';
import { InstructionComponent } from './pages/instruction/instruction.component';
import { ParentsInfoComponent } from './pages/parents-info/parents-info.component';
import { PhotoComponent } from './pages/photo/photo.component';
import {InstructionDoctorComponent} from './pages/instruction-doctor/instruction-doctor.component';

const routes: Routes = [
  {path: '', component: HomeComponent},
  {path: 'contact-us', component: ContactUsComponent},
  {path: 'certificates', component: CertificatesComponent},
  {path: 'ctg-sigmafon', component: CtgSigmafonComponent},
  {path: 'instruction', component: InstructionComponent},
  {path: 'parents-info', component: ParentsInfoComponent},
  {path: 'photo', component: PhotoComponent},
  {path: 'instruction-doctor', component: InstructionDoctorComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
