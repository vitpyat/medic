import { ApiService } from '../api.service';
import {Injectable} from '@angular/core';

@Injectable({
  providedIn: 'root'
})

export class PhotoService extends ApiService {
  // tslint:disable-next-line:variable-name
  protected _apiMethodToCall: (string) = 'getPhotos';

  public getPhotos() {
    return this.get(this._apiMethodToCall);
  }
}
