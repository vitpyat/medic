import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { throwError } from 'rxjs';
import {environment} from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})

export abstract class ApiService {
    // tslint:disable:variable-name
    protected _apiHost: (string) = '127.0.0.1';
    protected _apiPrefix: (string) = 'api';
    protected abstract _apiMethodToCall: (string);

    constructor(protected httpClient: HttpClient) {
        this.apiHost = environment.baseApiUrl;
    }

    /**
     * Get full API url
     */
    public getFullApiUrl(): string {
        let url = this.apiHost;
        if (url.indexOf('http://') === -1) {
            url = 'http://' + this.apiHost;
        }
        if (this.apiPrefix) {
            url += '/' + this.apiPrefix;
        }

        return url;
    }

    /**
     * Make a GET request
     *
     * @param method API method to get data
     */
    public get(method: string) {
        return this.httpClient.get(
            this.getFullApiUrl() + '/' + method,
            {
                responseType: 'json',
                withCredentials: false,
                observe: 'response'
            }
        ).pipe(
            catchError(
                (error) => {
                    let errorMessage = '';
                    if (error.error instanceof ErrorEvent) {
                        // client-side error
                        errorMessage = `Error: ${error.error.message}`;
                    } else {
                        // server-side error
                        errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
                    }
                    console.error(errorMessage);

                    return throwError(errorMessage);
                }
            )
        );
    }

    /**
     * Make a POST request
     *
     * @param method API method to get data
     */
    public post(method: string) {
        return this.httpClient.post(
            this.getFullApiUrl() + '/' + method,
            {}
        );
    }

    get apiHost(): string {
        return this._apiHost;
    }

    set apiHost(value: string) {
        this._apiHost = value;
    }

    get apiPrefix(): string {
        return this._apiPrefix;
    }

    set apiPrefix(value: string) {
        this._apiPrefix = value;
    }
}
