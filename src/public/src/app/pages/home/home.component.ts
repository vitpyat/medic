import { Component, OnInit } from '@angular/core';
import {IImage} from 'ng-simple-slideshow';
import { DomSanitizer, SafeUrl } from '@angular/platform-browser';

import {HomeApiService} from '../../service/home/home-api.service';
import {environment} from '../../../environments/environment';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  /**
   * Gallery to display (video images)
   */
  public galleryToDisplayVideo;
  public mainImage;

  constructor(
      protected homeService: HomeApiService,
      private sanitizer: DomSanitizer
  ) { }

  ngOnInit() {
    this.mainImage = environment.baseApiUrl + '/images/pregnant_family_hp.jpg';
    this.homeService.getHomeGallery().subscribe((response) => {
        if (response.body) {
          this.galleryToDisplayVideo = response.body;
          for (let i = 0; i <= this.galleryToDisplayVideo.length - 1; i++) {
            this.galleryToDisplayVideo[i] = this.sanitizer.bypassSecurityTrustResourceUrl(this.galleryToDisplayVideo[i].path);
          }
        }
    });
  }
}
