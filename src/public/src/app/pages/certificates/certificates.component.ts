import { Component, OnInit } from '@angular/core';
import {CertificatesService} from '../../service/certificates/certificates.service';

@Component({
  selector: 'app-certificates',
  templateUrl: './certificates.component.html',
  styleUrls: ['./certificates.component.scss']
})
export class CertificatesComponent implements OnInit {
  public certificates;

  constructor(protected certificateService: CertificatesService) { }

  ngOnInit() {
    this.certificateService.getCertificates().subscribe((response) => {
      if (response.body) {
        this.certificates = response.body;
      }
    });
  }

}
