import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CtgSigmafonComponent } from './ctg-sigmafon.component';

describe('CtgSigmafonComponent', () => {
  let component: CtgSigmafonComponent;
  let fixture: ComponentFixture<CtgSigmafonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CtgSigmafonComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CtgSigmafonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
