export const environment = {
  production: true,
  baseUrl: 'http://www.medsystems.com.ua',
  baseApiUrl: 'http://api.medsystems.com.ua'
};
